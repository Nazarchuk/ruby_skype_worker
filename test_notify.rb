require_relative 'lib/contact'
require_relative 'jobs/notify'

notifycation = {
  contact: {
    id: '1',
    skype: 'webdeluxe2014',
    email: 'me@evgen.in',
    phone: '+375293939910'
  },
  task: 'skype_message',
  issue: {
    id: 1,
    description: "Response code 300 not match 200",
    watcher: {
      id: 1,
      user_id: 1,
      uri: 'http://metricscat.com/',
      is_critical: 1,
      delay: 5,
      patterns: {
        pattern: '[:response_code:]',
        condition: '=',
        value: '200',
      }
    }
  },
  report_url: 'http://localhost:3000/issues/register_answer'
}

notifycation2 = notifycation.clone
notifycation3 = notifycation.clone

notifycation2[:task] = 'skype_call'
notifycation3[:task] = 'phone_call'

args = [notifycation, notifycation2,notifycation3]

notify = Notify.new
notify.perform args