require_relative '../lib/skype'
require_relative '../lib/contact'

class Notify
  RESULT_NOT_ANSWERED = 'NOT_ANSWERED'
  RESULT_ANSWERED = 'ANSWERED'
  ERROR_NO_PHONE_NUMBER = 'NO_PHONE_NUMBER'

  def initialize
    @callback_url = 'http://notifyninja.com/issues/register_answer'
    @in_call_audio = "hello.wav"
    @skype = false
    @yowsupCliPath = '/opt/yowsup/src/yowsup-cli'
    @yowsupConfigPath = 'home/skype-worker-ruby/config/yowsup.config'
    @skype = false
  end

  def perform(args)
    puts args
    args.each do |options|
      type = options[:task]
      puts type
      # begin
        result = process_notification type, options
        puts 'OK'
      # rescue Exception => e
        # puts 'Something wrong with worker'
        # next
      # end
      next if type == 'skype_message'
      data = {}
      data[:result] = result
      data.merge options
      if result == RESULT_ANSWERED and
        options[:onsuccess] == 'stop'
        puts 'COOL ANSWERED'
        break
      end
      ret = make_post options[:issue][:report_url],data
      puts data
      puts ret
      puts result
      return true if result == RESULT_ANSWERED
    end
  end

  def make_post(url, data)
    post_data= {
      http: {
        header: "Content-type: application/x-www-form-urlencoded\r\n",
        method: 'POST',
        content: http_build_query(data)
      }
    }
    context = stream_context_create(post_data)
    file_get_contents url, false, context
  end

  def process_notification(type, options)
    contact = Contact.new options[:contact]
    @skype = init_skype contact
    result = RESULT_NOT_ANSWERED
    case type
    when 'skype_call'
      result = call options, true
    when 'skype_message'
      @skype.send_message options[:issue][:description]
    when 'phone_call'
      result = call options, false
    when 'sms'
      @skype.send_SMS options[:issue][:description]
    when 'email'
      subject = defined? options[:issue][:subject] ?options[:issue][:description] : "Ninja reports you"
      mail options[:email], subject, options[:issue][:description]
    when 'watsapp_message'
      cliPath = @yowsupCliPath
      configPath = @yowsupConfigPath
      system "#{cliPath} -c #{configPath} -s " + options[:phone] + " " + options[:message]
    when 'add_to_skype'
      @skype.add_baddy options[:issue][:description]
    else
      raise Exception.new 'Please check task of the job - ' + type  
    end
    result
  end

  private

  def init_skype(contact = false)
    unless @skype
      @skype = Skype.new contact
    end
    @skype
  end

  def verify(skype)
    verify_status = false
    5.times do
      status = skype.call_status
      puts status 
      case status
      when Skype::CALL_STATUS_INPROGRESS
        puts "INPROGRESS"
        skype.play_sound @in_call_audio
        sleep 3
        skype.end_call
        verify_status = true
      when Skype::CALL_STATUS_VM_PLAYING_GREETING,
        Skype::CALL_STATUS_VM_RECORDING
        skype.end_call
      when Skype::CALL_STATUS_CANCELLED,
        Skype::CALL_STATUS_REFUSED,
        Skype::CALL_STATUS_FAILED
        puts "CANCELLED"
        verify_status = false
      when Skype::CALL_STATUS_FINISHED
        puts "FINISHED"
        verify_status = false
      end
      sleep 1
    end
    verify_status
  end

  def call(options, is_skype = true)
    contact = Contact.new options[:contact]
    @skype = init_skype contact
    res = ''
    if is_skype
      @skype.make_skype_call
      res = check_call_status true
    else
      @skype.make_phone_call
      res = check_call_status false
    end
    res
  end

  def check_call_status(is_skype_call = true)
    res = ''
    i = 0
    skype = init_skype
    while i < 3
      if verify(skype)
        resul = RESULT_ANSWERED
        skype.send_message 'Problem approved!'
        break
      end
      i += 1
    end
    if i >= 3
      if is_skype_call
        skype.send_message 'Problem NOT approved! (ninja) call to mobile'
      else
        skype.send_message 'Problem NOT approved! (ninja) calling cycle'
      end
      skype.end_call
      res = RESULT_NOT_ANSWERED
    end
    res
  end
end