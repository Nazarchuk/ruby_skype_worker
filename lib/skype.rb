require 'dbus'
require_relative 'contact'

class Skype
  attr_accessor :contact

  CALL_STATUS_ROUTING = 'ROUTING'
  CALL_STATUS_UNPLACED = 'UNPLACED'
  CALL_STATUS_RINGING = 'RINGING'
  CALL_STATUS_INPROGRESS = 'INPROGRESS'
  CALL_STATUS_FINISHED = 'FINISHED'
  CALL_STATUS_CANCELLED = 'CANCELLED'
  CALL_STATUS_BUSY = 'BUSY'
  CALL_STATUS_FAILED = 'FAILED'
  CALL_STATUS_REFUSED = 'REFUSED'
  CALL_STATUS_EARLYMEDIA = 'EARLYMEDIA'
  CALL_STATUS_VM_PLAYING_GREETING = 'VM_PLAYING_GREETING';
  CALL_STATUS_VM_RECORDING = 'VM_RECORDING'
  CALL_STATUS_VM_SENT = 'VM_SENT'

  @@proxy = false

  def initialize(contact)
    @chat_id = @call_id = false
    @contact = contact
    bus = DBus::SessionBus.instance
    service = bus.service "com.Skype.API"
    skype = service.object "/com/Skype"
    skype.introspect
    @@proxy = skype
    connect
  end

  def connect
    query 'NAME RUBY'
    query 'PROTOCOL 7'
  end

  def make_skype_call
    res = query 'CALL ' + contact.skype
    _, @call_id = res.first.split
  end

  def end_call
    res = query 'ALTER CALL ' + @call_id + ' HANGUP'
  end

  def make_phone_call
    query 'CALL ' + contact.phone
    _, @call_id = res.first.split
  end

  def call_status
    res = query 'GET CALL ' + @call_id + ' STATUS'
    reply, _, _, status = res.first.split
    if reply == 'ERROR'
      return CALL_STATUS_FAILED
    end
    status
  end

  def check_status
    res = query 'PING'
    res == 'PONG'
  end

  def send_message msg
    create_chat
    query 'CHATMESSAGE ' + @chat_id + ' ' + msg
  end

  def send_SMS msg
    res = query 'CREATE SMS OUTGOING ' + contact.phone
    id = res.split[1]
    query "SET SMS " + id + " BODY /"" + msg "/""
    query 'ALTER SMS ' + id + ' SEND'
  end

  def play_sound path
    query "ALTER CALL " + @call_id + " SET_INPUT file=\"../" + path + "\""
  end

  def add_baddy msg
    query 'SET USER ' + contact.skype + ' BUDDYSTATUS 2 ' + msg
  end

  private

  def query(query)
    @@proxy.Invoke(query)
  end

  def create_chat
    res = query 'CHAT CREATE ' + contact.skype
    _, id = res.first.split
    @chat_id = id
  end

  def get_chat_id
    unless chat_id
      return create_chat
    end
  end
end
