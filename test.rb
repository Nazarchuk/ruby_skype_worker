require_relative 'lib/skype'
require_relative 'lib/contact'

contact = Contact.new({
  skype: 'webdeluxe2014',
  email: 'me@evgen.in',
  phone: '+375293939910'})

def verify(skype)
  verify_status = false
  120.times do
    status = skype.call_status
    case status
    when Skype::CALL_STATUS_INPROGRESS
      puts "INPROGRESS"
      skype.end_call
    when Skype::CALL_STATUS_CANCELLED, Skype::CALL_STATUS_REFUSED
      puts "CANCELLED"
    when Skype::CALL_STATUS_FINISHED
      puts "FINISHED"
      break verify_status = true
    end
    sleep 1
  end
  return verify_status
end

skype = Skype.new contact
skype.connect
skype.send_message 'We have a problem!'
skype.make_skype_call

3.times do
  if verify(skype)
    skype.send_message 'Problem approved!'
    break
  end
end

skype.send_message 'Problem NOT approved!'